def test_passwd_file(host):
    passwd = host.file("/etc/passwd2")
    assert passwd.contains("root")
    assert passwd.user == "root"
    assert passwd.group == "root"
    assert passwd.mode == 0o644
